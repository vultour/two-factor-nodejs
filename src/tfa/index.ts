import * as redis from 'redis';
import * as crypto from 'crypto';


export enum ResponseStatus{ SUCCESS, ERROR };
export class Response{ public constructor(public status: ResponseStatus, public content: any){} };

export default class TFA{
    private db: redis.RedisClient;
    
    private hashValidity: number;
    private hashAlgo: string;
    private hashLength: number;


    public constructor(hashValidity: number, hashAlgo: string, hashLength: number){
        this.db = redis.createClient();
        this.db.on("error", function(err){
            console.log("REDIS ERROR:", err);
        });

        this.hashValidity   = hashValidity;
        this.hashAlgo       = hashAlgo;
        this.hashLength     = hashLength;
    }

    private generateTimestamp(){
        return Math.floor(new Date().getTime() / 1000);
    }

    public createUser (username: string, callback: (Response) => void){
        this.db.get(username, (err, reply) => {
            console.log(err, reply);
            if (reply === null){
                let hash = crypto.createHash("sha256");
                hash.update(crypto.randomBytes(128));
                this.db.set(username, this.generateTimestamp());
                this.db.set(username + "_salt", hash.digest("hex"));
                callback(new Response(ResponseStatus.SUCCESS, "User created successfuly"));
            } else{
                callback(new Response(ResponseStatus.ERROR, "User already exists"));
            }
        });
    }

    public generate(username: string, callback: (Response) => void){
        this.db.get(username, (err, reply) => {
            if (reply === null){
                callback(new Response(ResponseStatus.SUCCESS, "Username doesn't exist"));
            } else{
                let epoch = parseInt(reply);
                this.db.get(username + "_salt", (err, reply) => {
                    if (reply === null){
                        callback(new Response(ResponseStatus.SUCCESS, "Fatal error"));
                    } else{
                        let hash = crypto.createHash(this.hashAlgo);
                        let salt = reply;
                        let iteration = Math.floor((this.generateTimestamp() - epoch) / this.hashValidity);
                        hash.update(salt + iteration);
                        let authCode = hash.digest("hex").substr(0, this.hashLength);
                        callback(new Response(ResponseStatus.SUCCESS, {code: authCode, it: iteration}));
                    }
                }); 
            }
        });
    }

    public check(username: string, code: string, callback: (Response) => void){
        this.generate(username, (resp: Response) => {
            if (resp.status != ResponseStatus.SUCCESS){
                callback(resp);
            } else {
                let correctCode = resp.content.code;
                if (code == correctCode){
                    callback(new Response(ResponseStatus.SUCCESS, {match: true}));
                } else{
                    callback(new Response(ResponseStatus.SUCCESS, {match: false}));
                }
            }
        });
    }
}