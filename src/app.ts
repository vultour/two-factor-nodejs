/// <reference path="../typings/index.d.ts" />

import * as express from "express";
import {default as TFA} from './tfa';


let app = express();
let twofactor = new TFA(15, "sha256", 5);

app.get("/", function(req, res){
    res.end(
        "<!DOCTYPE html><head></head><body>" +
        
        "<h1>Service gateway</h1>" +
        "<h3>Register<h3><tt>POST /gateway/register/</tt>" +
        "<h3>Generate API token</h3><tt>POST /gateway/generate/</tt>" +
        "<h3>Verify user code</h3><tt>POST /gateway/verify/</tt>" +

        "<h1>User endpoints</h1>" +
        "<h3>Create user</h3><tt>POST /users/create/</tt>" +
        "<h3>Get user<h3><tt>GET /user/info/&lt;username&gt;/</tt>" +
        "<h3>Get current user</h3><tt>POST /user/me/</tt>"
    );
});

app.get("/create/:user", function(req, res){
    twofactor.createUser(req.params.user, function(response){
        res.json({req: req.params, resp: response});
    });
});

app.get("/generate/:user", function(req, res){
    twofactor.generate(req.params.user, function(response){
        res.json(response);
    });
});

app.get("/check/:user/:code", function(req, res){
    twofactor.check(req.params.user, req.params.code, (response) => {
        res.json(response);
    });
});


let server = app.listen(7777, function(){
    console.log("Listening on", server.address().address, server.address().port);
});
